import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:empresas_flutter/main.dart';

void main() {
  testWidgets('Testar validação do email', (WidgetTester tester) async {
    String emailInvalido = 'testeapple@';
    String senha = '12341234';

    await tester.pumpWidget(const MyApp());

    final emailTextFieldFinder = find.byKey(ValueKey('emailTextFormField'));
    final senhaTextFieldFinder = find.byKey(ValueKey('senhaTextFormField'));
    final botaoEntrarFinder = find.text('ENTRAR');

    // Verificando se os dois TextFormField existe.
    expect(emailTextFieldFinder, findsOneWidget);
    expect(senhaTextFieldFinder, findsOneWidget);

    // Escrevendo o email e senha nos TextFormFields.
    // await tester.tap(emailTextFieldFinder);
    await tester.enterText(emailTextFieldFinder, emailInvalido);
    await tester.enterText(senhaTextFieldFinder, senha);

    // Fazendo o login.
    await tester.pumpAndSettle();
    await tester.tap(botaoEntrarFinder);

    // Adicionando uma espera.
    await tester.pump(const Duration(milliseconds: 100));

    // Procurando pela mensagem de email inválido.
    expect(find.text('Endereço de email inválido'), findsOneWidget);
  });
}