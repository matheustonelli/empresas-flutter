import 'package:empresas_flutter/utils/validator/validator.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('Teste do validador de email e senha', (){

    test('validateEmail_EmailValido_Sucesso', (){
      String email = 'testeapple@ioasys.com.br';

      var result = Validator.validateEmail(email);

      expect(result, null);
    });

    test('validateEmail_EmailInvalido_MensagemErro', (){
      String email = 'testeapple@';

      var result = Validator.validateEmail(email);

      expect(result, 'Endereço de email inválido');
    });

    test('validateEmail_EmailVazio_MensagemErro', (){
      String email = '';

      var result = Validator.validateEmail(email);

      expect(result, 'Por favor insira seu email');
    });

    test('validateSenha_SenhaValida_Sucesso', (){
      String senha = '12341234';

      var result = Validator.validatePassword(senha);

      expect(result, null);
    });

    test('validateSenha_SenhaVazio_MensagemErro', (){
      String senha = '';

      var result = Validator.validatePassword(senha);

      expect(result, 'Por favor insira sua senha');
    });
  });
}