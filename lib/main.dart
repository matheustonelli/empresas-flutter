import 'package:empresas_flutter/constants/app_colors.dart';
import 'package:empresas_flutter/pages/login/login_page.dart';
import 'package:flutter/material.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Empresas ioasys',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: AppColors.primaria,
        // fontFamily: 'Gilroy',
        colorScheme: ColorScheme.light(
          primary: AppColors.primaria,
          secondary: AppColors.secundaria,
          onPrimary: AppColors.preto,
          onSecondary: AppColors.branco,
          background: AppColors.branco,
          onBackground: AppColors.preto,
          error: AppColors.erroText,
          onError: AppColors.branco,
        ),
      ),
      home: const LoginPage(),
    );
  }
}
