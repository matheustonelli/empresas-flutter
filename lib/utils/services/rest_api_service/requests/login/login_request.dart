import 'package:empresas_flutter/constants/api_paths.dart';
import 'package:empresas_flutter/domain/entities/usuario/usuario.dart';
import 'package:empresas_flutter/domain/interfaces/http_service.dart';
import 'package:empresas_flutter/utils/services/rest_api_service/http/http_service.dart';
import 'package:http/http.dart';

class LoginRequest {
  static final IHttpService _httpService = HttpService();

  Future<Usuario?> autenticar(String email, String senha) async {
    try {
      Map<String, dynamic> body = {
        'email': email,
        'password': senha,
      };

      var response = await _httpService.post(ApiPaths.login, body);
      return criarUsuario(response);
    } catch (e) {
      return null;
    }
  }

  Usuario criarUsuario(Response response) {
    var header = response.headers;
    Usuario usuario = Usuario(
      header['access-token']!,
      header['client']!,
      header['uid']!,
    );
    return usuario;
  }
}
