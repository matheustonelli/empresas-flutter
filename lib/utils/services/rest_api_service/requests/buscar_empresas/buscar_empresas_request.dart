import 'dart:convert';
import 'package:empresas_flutter/constants/api_paths.dart';
import 'package:empresas_flutter/domain/entities/empresa/empresa.dart';
import 'package:empresas_flutter/domain/entities/usuario/usuario.dart';
import 'package:empresas_flutter/domain/interfaces/http_service.dart';
import 'package:empresas_flutter/utils/services/rest_api_service/http/http_service.dart';

class BuscarEmpresasRequest {
  static final IHttpService _httpService = HttpService();

  Future<List<Empresa>> buscarEmpresas(String pesquisa, Usuario? usuario) async {
    if (usuario == null || pesquisa.isEmpty) {
      return [];
    }

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'access-token': usuario.accessToken,
      'client': usuario.client,
      'uid': usuario.uid,
    };

    Map<String, String> query = {
      'name': pesquisa,
    };

    var result = await _httpService.get(
      ApiPaths.buscarEmpresas,
      headers,
      query,
    );

    var resultDecoded = json.decode(result.body) as Map<String, dynamic>;
    var empresasJson = resultDecoded['enterprises'] as List;
    List<Empresa> empresas = empresasJson.map((empresa) => Empresa.fromMap(empresa)).toList();

    return empresas;
  }
}
