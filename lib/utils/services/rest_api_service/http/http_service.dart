import 'dart:io';
import 'package:empresas_flutter/constants/api_paths.dart';
import 'package:http/http.dart' as http;
import 'package:empresas_flutter/domain/interfaces/http_service.dart';
import 'package:http/http.dart';

class HttpService implements IHttpService {
  @override
  Future<Response> get(String url, Map<String, String>? headers, Map<String, dynamic>? query) async {
    try {
      Uri baseUrl = Uri(
        scheme: ApiPaths.scheme,
        host: ApiPaths.host,
        path: url,
        queryParameters: query,
      );
      final response = await http.get(
        baseUrl,
        headers: headers,
      );
      return responseResult(response);
    } on SocketException catch (_) {
      throw Exception('Sem conexão com a internet');
    }
  }

  @override
  Future<Response> post(String url, dynamic body) async {
    try {
      Uri baseUrl = Uri(
        scheme: ApiPaths.scheme,
        host: ApiPaths.host,
        path: url,
      );

      final response = await http.post(
        baseUrl,
        body: body,
      );
      return responseResult(response);
    } on SocketException catch (_) {
      throw Exception('Sem conexão com a internet');
    }
  }

  Response responseResult(http.Response response) {
    if (response.statusCode == 200) {
      return response;
    } else {
      throw Exception('Erro: ${response.body.toString()}');
    }
  }
}
