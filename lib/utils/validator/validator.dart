import 'package:email_validator/email_validator.dart';

class Validator {

  static String? validatePassword(String? value){
    if (value == null || value.isEmpty) {
      return 'Por favor insira sua senha';
    }
    return null;
  }

  static String? validateEmail(String? value){
    if (value == null || value.isEmpty) {
      return 'Por favor insira seu email';
    } else if (!EmailValidator.validate(value)) {
      return 'Endereço de email inválido';
    }
    return null;
  }
}