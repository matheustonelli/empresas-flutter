import 'package:empresas_flutter/pages/app_widgets/carregando/carregando.dart';
import 'package:empresas_flutter/pages/app_widgets/login_invalido/login_invalido.dart';
import 'package:flutter/material.dart';

class AppDialogs {

  static void loginInvalidoDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return const LoginInvalido();
      },
    );
  }

  static void carregandoDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return const Carregando();
      },
    );
  }
}