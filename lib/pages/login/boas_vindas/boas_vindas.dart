import 'package:empresas_flutter/constants/app_colors.dart';
import 'package:flutter/material.dart';

class BoasVindas extends StatelessWidget {
  const BoasVindas({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: LayoutBuilder(
        builder: (context, constraints) {
          return Padding(
            padding: EdgeInsets.fromLTRB(16.0, 0.0, 0.0, constraints.maxHeight * 0.1),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                Text(
                  "Boas vindas,",
                  style: TextStyle(
                    color: AppColors.branco,
                    fontSize: 40,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  "Você está no Empresas.",
                  style: TextStyle(
                    color: AppColors.branco,
                    fontSize: 25,
                    fontWeight: FontWeight.w300,
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
