import 'package:empresas_flutter/pages/app_widgets/background/background.dart';
import 'package:empresas_flutter/pages/login/boas_vindas/boas_vindas.dart';
import 'package:empresas_flutter/pages/login/input_login/input_login.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Background(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            BoasVindas(),
            InputLogin(),
          ],
        ),
      ),
    );
  }
}
