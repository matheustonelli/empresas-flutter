import 'package:empresas_flutter/constants/app_colors.dart';
import 'package:flutter/material.dart';

class LoginTextField extends StatefulWidget {
  final TextEditingController controller;
  final String hintText;
  final String label;
  final Function onChanged;
  final FormFieldValidator<String> validator;
  final bool isPassword;

  const LoginTextField({
    Key? key,
    required this.controller,
    required this.hintText,
    required this.label,
    required this.onChanged,
    required this.validator,
    this.isPassword = false,
  }) : super(key: key);

  @override
  State<LoginTextField> createState() => _LoginTextFieldState();
}

class _LoginTextFieldState extends State<LoginTextField> {
  bool _showPassword = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.isPassword ? EdgeInsets.zero : EdgeInsets.only(bottom: 16.0),
      child: TextFormField(
        controller: widget.controller,
        obscureText: widget.isPassword ? !_showPassword : false,
        decoration: InputDecoration(
          hintText: widget.hintText,
          labelText: widget.label,
          labelStyle: const TextStyle(
            color: AppColors.labelTextField,
          ),
          floatingLabelStyle: const TextStyle(
            color: AppColors.labelTextField,
            fontWeight: FontWeight.bold,
            fontSize: 18,
          ),
          border: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
            borderSide: BorderSide(
              color: AppColors.textFieldBorder,
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
            borderSide: BorderSide(
              color: AppColors.secundaria,
              width: 1.5,
            ),
          ),
          errorBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
            borderSide: BorderSide(
              color: AppColors.erroBorder,
              width: 1.5,
            ),
          ),
          errorStyle: const TextStyle(
            color: AppColors.erroText,
          ),
          suffixIcon: widget.isPassword
              ? GestureDetector(
                  onTap: () => _togglePasswordVisibility(),
                  child: Icon(
                    _showPassword ? Icons.visibility : Icons.visibility_off,
                    color: AppColors.mostrarSenha,
                  ),
                )
              : null,
        ),
        cursorColor: AppColors.preto,
        cursorHeight: 25,
        cursorWidth: 0.75,
        onChanged: (text) => widget.onChanged(),
        validator: widget.validator,
      ),
    );
  }

  _togglePasswordVisibility() {
    setState(() {
      _showPassword = !_showPassword;
    });
  }
}
