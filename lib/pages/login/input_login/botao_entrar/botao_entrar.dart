import 'package:empresas_flutter/constants/app_colors.dart';
import 'package:flutter/material.dart';

class BotaoEntrar extends StatefulWidget {
  final VoidCallback? onPressed;

  const BotaoEntrar({
    Key? key,
    required this.onPressed,
  }) : super(key: key);

  @override
  State<BotaoEntrar> createState() => _BotaoEntrarState();
}

class _BotaoEntrarState extends State<BotaoEntrar> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: ElevatedButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.resolveWith<Color>(
                (Set<MaterialState> states) {
                  if (states.contains(MaterialState.pressed)) {
                    return AppColors.primaria;
                  } else if (states.contains(MaterialState.disabled)) {
                    return AppColors.desativado;
                  }
                  return AppColors.primaria;
                },
              ),
              shape: MaterialStateProperty.all<OutlinedBorder>(
                const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(45),
                  ),
                ),
              ),
            ),
            child: const Padding(
              padding: EdgeInsets.symmetric(vertical: 16.0),
              child: Text(
                "ENTRAR",
                style: TextStyle(color: AppColors.branco, fontSize: 16),
              ),
            ),
            onPressed: widget.onPressed,
          ),
        ),
      ],
    );
  }
}
