import 'package:empresas_flutter/constants/app_colors.dart';
import 'package:empresas_flutter/core/auth/login/login.dart';
import 'package:empresas_flutter/pages/login/input_login/botao_entrar/botao_entrar.dart';
import 'package:empresas_flutter/pages/login/input_login/login_text_field/login_text_field.dart';
import 'package:empresas_flutter/utils/validator/validator.dart';
import 'package:flutter/material.dart';

class InputLogin extends StatefulWidget {
  const InputLogin({Key? key}) : super(key: key);

  @override
  State<InputLogin> createState() => _InputLoginState();
}

class _InputLoginState extends State<InputLogin> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController emailController = TextEditingController();
  TextEditingController senhaController = TextEditingController();

  @override
  void dispose() {
    emailController.dispose();
    senhaController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.5,
      padding: EdgeInsets.symmetric(
        vertical: MediaQuery.of(context).size.height * 0.03,
        horizontal: MediaQuery.of(context).size.width * 0.05,
      ),
      decoration: const BoxDecoration(
        color: AppColors.branco,
      ),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Digite seus dados para continuar.",
              style: TextStyle(
                  color: AppColors.preto,
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  LoginTextField(
                    key: ValueKey('emailTextFormField'),
                    controller: emailController,
                    label: 'Email',
                    hintText: 'Email',
                    validator: Validator.validateEmail,
                    onChanged: () {
                      setState(() {});
                    },
                  ),
                  LoginTextField(
                    key: ValueKey('senhaTextFormField'),
                    controller: senhaController,
                    label: 'Senha',
                    hintText: 'Senha',
                    validator: Validator.validatePassword,
                    onChanged: () {
                      setState(() {});
                    },
                    isPassword: true,
                  ),
                ],
              ),
            ),
            BotaoEntrar(
              onPressed: habilitarBotaoEntrar()
                  ? () => Login.login(
                        context,
                        _formKey,
                        emailController.text,
                        senhaController.text,
                      )
                  : null,
            ),
          ],
        ),
      ),
    );
  }

  bool habilitarBotaoEntrar() {
    return emailController.text.isNotEmpty && senhaController.text.isNotEmpty;
  }
}
