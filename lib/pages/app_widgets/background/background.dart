import 'package:empresas_flutter/constants/app_images.dart';
import 'package:flutter/material.dart';

class Background extends StatelessWidget {
  final Widget child;

  const Background({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image.asset(
          AppImages.backgroundLogin,
          fit: BoxFit.cover,
          height: double.infinity,
          width: double.infinity,
          alignment: Alignment.center,
        ),
        Positioned(
          right: 0.0,
          top: MediaQuery.of(context).size.height * 0.23,
          child: Image.asset(AppImages.logoIoasysVector),
        ),
        child,
      ],
    );
  }
}
