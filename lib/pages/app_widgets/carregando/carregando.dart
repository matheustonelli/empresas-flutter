import 'package:empresas_flutter/constants/app_colors.dart';
import 'package:flutter/material.dart';

class Carregando extends StatelessWidget {
  const Carregando({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.5),
      ),
      height: double.infinity,
      width: double.infinity,
      child: const Center(
        child: CircularProgressIndicator(
          color: AppColors.primaria,
        ),
      ),
    );
  }
}
