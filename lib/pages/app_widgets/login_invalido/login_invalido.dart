import 'package:empresas_flutter/constants/app_colors.dart';
import 'package:flutter/material.dart';

class LoginInvalido extends StatelessWidget {
  const LoginInvalido({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      title: const Text('Email ou senha incorretos'),
      children: [
        TextButton(
          onPressed: () => Navigator.pop(context),
          child: const Text(
            "OK",
            style: TextStyle(
              color: AppColors.primaria,
            ),
          ),
        )
      ],
    );
  }
}
