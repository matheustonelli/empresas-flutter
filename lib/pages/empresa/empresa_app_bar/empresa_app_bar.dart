import 'package:empresas_flutter/constants/app_colors.dart';
import 'package:empresas_flutter/constants/app_images.dart';
import 'package:empresas_flutter/domain/entities/empresa/empresa.dart';
import 'package:flutter/material.dart';

class EmpresaAppBar extends StatelessWidget {
  final Empresa empresa;

  const EmpresaAppBar({
    Key? key,
    required this.empresa,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      slivers: [
        SliverAppBar(
          floating: false,
          centerTitle: true,
          automaticallyImplyLeading: false,
          backgroundColor: Colors.transparent,
          expandedHeight: MediaQuery.of(context).size.height * 0.2,
          flexibleSpace: Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage(AppImages.background),
                fit: BoxFit.cover,
              ),
            ),
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.bottomLeft,
                  child: IconButton(
                    onPressed: () => Navigator.pop(context),
                    iconSize: 26.0,
                    padding: const EdgeInsets.only(left: 24.0, bottom: 24.0),
                    icon: const Icon(
                      Icons.arrow_back_rounded,
                      color: AppColors.branco,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 24.0),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          empresa.nome,
                          style: const TextStyle(
                            color: AppColors.branco,
                            fontWeight: FontWeight.bold,
                            fontSize: 24,
                          ),
                        ),
                        Text(
                          empresa.tipoEmpresa,
                          style: const TextStyle(
                            color: AppColors.branco,
                            fontWeight: FontWeight.w300,
                            fontSize: 18,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
