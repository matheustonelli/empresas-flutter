import 'package:empresas_flutter/constants/app_colors.dart';
import 'package:flutter/material.dart';

class EmpresaDescricao extends StatelessWidget {
  final String descricao;

  const EmpresaDescricao({
    Key? key,
    required this.descricao,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16.0, 24.0, 16.0, 16.0),
      child: Text(
        descricao,
        style: const TextStyle(
          color: AppColors.empresaNome,
          fontSize: 20,
          fontWeight: FontWeight.w300
        ),
      ),
    );
  }
}
