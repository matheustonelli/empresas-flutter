import 'package:empresas_flutter/constants/api_paths.dart';
import 'package:flutter/material.dart';

class EmpresaFoto extends StatelessWidget {
  final String foto;

  const EmpresaFoto({
    Key? key,
    required this.foto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String imageUrl = ApiPaths.fotoEmpresa + foto;

    return Container(
      height: MediaQuery.of(context).size.height * 0.38,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: NetworkImage(imageUrl),
          fit: BoxFit.cover,
        ),
        borderRadius: const BorderRadius.only(
          bottomLeft: Radius.circular(16),
          bottomRight: Radius.circular(16),
        ),
      ),
    );
  }
}
