import 'package:empresas_flutter/domain/entities/empresa/empresa.dart';
import 'package:empresas_flutter/pages/empresa/empresa_app_bar/empresa_app_bar.dart';
import 'package:empresas_flutter/pages/empresa/empresa_descricao/empresa_descricao.dart';
import 'package:empresas_flutter/pages/empresa/empresa_foto/empresa_foto.dart';
import 'package:flutter/material.dart';



class EmpresaPage extends StatelessWidget {
  final Empresa empresa;

  const EmpresaPage({
    Key? key,
    required this.empresa,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            EmpresaAppBar(empresa: empresa),
            EmpresaFoto(foto: empresa.foto),
            EmpresaDescricao(descricao: empresa.descricao),
          ],
        ),
      )
    );
  }
}
