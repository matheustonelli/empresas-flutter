import 'package:empresas_flutter/constants/app_colors.dart';
import 'package:empresas_flutter/constants/app_images.dart';
import 'package:flutter/material.dart';

class EmpresaNaoEncontrada extends StatelessWidget {
  const EmpresaNaoEncontrada({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 16.0),
            child: Image.asset(
              AppImages.empresaNaoEncontrada,
            ),
          ),
          Text(
            "Empresa não encontrada",
            style: TextStyle(
              color: AppColors.searchText.withOpacity(0.6),
              fontSize: 18,
              fontWeight: FontWeight.w400,
            ),
          ),
        ],
      ),
    );
  }
}
