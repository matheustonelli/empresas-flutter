import 'package:empresas_flutter/constants/app_colors.dart';
import 'package:empresas_flutter/domain/entities/empresa/empresa.dart';
import 'package:empresas_flutter/domain/entities/usuario/usuario.dart';
import 'package:empresas_flutter/pages/home/lista_empresas/empresa_nao_encontrada/empresa_nao_encontrada.dart';
import 'package:empresas_flutter/pages/home/lista_empresas/empresa_tile/empresa_tile.dart';
import 'package:empresas_flutter/utils/services/rest_api_service/requests/buscar_empresas/buscar_empresas_request.dart';
import 'package:flutter/material.dart';

class ListaEmpresas extends StatefulWidget {
  final String pesquisa;
  final Usuario usuario;

  const ListaEmpresas({
    Key? key,
    required this.pesquisa,
    required this.usuario,
  }) : super(key: key);

  @override
  State<ListaEmpresas> createState() => _ListaEmpresasState();
}

class _ListaEmpresasState extends State<ListaEmpresas> {
  BuscarEmpresasRequest buscarEmpresasRequest = BuscarEmpresasRequest();

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: FutureBuilder(
        future: buscarEmpresasRequest.buscarEmpresas(
          widget.pesquisa,
          widget.usuario,
        ),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
              List<Empresa> empresas = snapshot.data as List<Empresa>;

              if (empresas.isNotEmpty) {
                return ScrollConfiguration(
                  behavior: ScrollBehavior().copyWith(overscroll: false),
                  child: GridView.builder(
                    gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: 200,
                      childAspectRatio: 155 / 144,
                      crossAxisSpacing: 30,
                      mainAxisSpacing: 25,
                    ),
                    padding: const EdgeInsets.all(16.0),
                    itemCount: empresas.length,
                    itemBuilder: (context, index) {
                      return EmpresaTile(
                        empresa: empresas[index],
                      );
                    },
                  ),
                );
              } else if (empresas.isEmpty && widget.pesquisa.isNotEmpty) {
                return const EmpresaNaoEncontrada();
              }
            } else {
              Container();
            }
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: AppColors.primaria,
              ),
            );
          }
          return Container();
        },
      ),
    );
  }
}
