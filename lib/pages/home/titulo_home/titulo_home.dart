import 'package:empresas_flutter/pages/home/titulo_home/titulo_inicial/titulo_inicial.dart';
import 'package:empresas_flutter/pages/home/titulo_home/titulo_secundario/titulo_secundario.dart';
import 'package:flutter/material.dart';

class TituloHome extends StatefulWidget {
  final String pesquisa;
  final Function onPressed;

  const TituloHome({
    Key? key,
    required this.pesquisa,
    required this.onPressed,
  }) : super(key: key);

  @override
  State<TituloHome> createState() => _TituloHomeState();
}

class _TituloHomeState extends State<TituloHome> {
  Widget _child = TituloInicial();

  @override
  void initState() {
    super.initState();
    _child = TituloInicial();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.pesquisa.isEmpty) {
      _child = TituloInicial();
    } else {
      _child = TituloSecundario(onPressed: widget.onPressed);
    }

    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.2,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: AnimatedSwitcher(
              duration: const Duration(milliseconds: 1000),
              transitionBuilder: (child, animation) => SlideTransition(
                child: child,
                position: Tween<Offset>(
                  begin: Offset(-2, -1),
                  end: Offset(0, 0),
                ).animate(animation),
              ),
              switchInCurve: Curves.easeIn,
              switchOutCurve: Curves.easeIn,
              child: _child,
            ),
          ),
        ],
      ),
    );
  }
}
