import 'package:empresas_flutter/constants/app_colors.dart';
import 'package:flutter/material.dart';

class TituloSecundario extends StatelessWidget {
  final Function onPressed;

  const TituloSecundario({
    Key? key,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Align(
          alignment: Alignment.bottomCenter,
          child: Text(
            "Pesquise",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: AppColors.primaria,
              fontSize: 28,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: IconButton(
            onPressed: () => onPressed(),
            iconSize: 26.0,
            padding: const EdgeInsets.only(bottom: 12.0),
            icon: const Icon(
              Icons.arrow_back_rounded,
              color: AppColors.secundaria,
            ),
          ),
        ),
      ],
    );
  }
}
