import 'package:empresas_flutter/constants/app_colors.dart';
import 'package:flutter/material.dart';

class TituloInicial extends StatelessWidget {
  const TituloInicial({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      'Pesquise por\numa empresa',
      style: TextStyle(
        color: AppColors.primaria,
        fontSize: 40,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
