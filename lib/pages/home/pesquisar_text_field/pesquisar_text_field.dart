import 'package:empresas_flutter/constants/app_colors.dart';
import 'package:flutter/material.dart';

class PesquisarTextField extends StatefulWidget {
  final TextEditingController controller;
  final ValueChanged<String> onChanged;

  const PesquisarTextField({
    Key? key,
    required this.controller,
    required this.onChanged,
  }) : super(key: key);

  @override
  State<PesquisarTextField> createState() => _PesquisarTextFieldState();
}

class _PesquisarTextFieldState extends State<PesquisarTextField> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 24.0),
      child: TextFormField(
        controller: widget.controller,
        textInputAction: TextInputAction.search,
        onEditingComplete: () {
          FocusScope.of(context).unfocus();
          widget.onChanged(widget.controller.text);
        },
        decoration: InputDecoration(
          label: Row(
            children: const [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.0),
                child: Icon(Icons.search, color: AppColors.searchIcon, size: 30),
              ),
              Text(
                'Buscar...',
                style: TextStyle(
                  color: AppColors.searchText,
                ),
              ),
            ],
          ),
          floatingLabelBehavior: FloatingLabelBehavior.never,
          border: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
            borderSide: BorderSide(
              color: AppColors.textFieldBorder,
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
            borderSide: BorderSide(
              color: AppColors.secundaria,
              width: 1.5,
            ),
          ),
        ),
        cursorColor: AppColors.preto,
        cursorHeight: 25,
        cursorWidth: 0.75,
      ),
    );
  }
}
