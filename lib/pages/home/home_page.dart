import 'package:empresas_flutter/constants/app_colors.dart';
import 'package:empresas_flutter/domain/entities/usuario/usuario.dart';
import 'package:empresas_flutter/pages/home/lista_empresas/lista_empresas.dart';
import 'package:empresas_flutter/pages/home/pesquisar_text_field/pesquisar_text_field.dart';
import 'package:empresas_flutter/pages/home/titulo_home/titulo_home.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  final Usuario usuario;

  const HomePage({
    Key? key,
    required this.usuario,
  }) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController pesquisaController = TextEditingController();
  String _pesquisa = '';

  @override
  void dispose() {
    pesquisaController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.branco,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TituloHome(
            pesquisa: _pesquisa,
            onPressed: () {
              pesquisaController.clear();
              setState(() {
                _pesquisa = pesquisaController.text;
              });
            },
          ),
          PesquisarTextField(
            controller: pesquisaController,
            onChanged: (pesquisa) {
              setState(() {
                _pesquisa = pesquisa;
              });
            },
          ),
          ListaEmpresas(
            pesquisa: _pesquisa,
            usuario: widget.usuario,
          ),
        ],
      ),
    );
  }
}
