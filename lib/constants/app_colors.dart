import 'package:flutter/material.dart';

class AppColors {
  static const Color preto = Color(0xFF000000);
  static const Color branco = Color(0xFFFFFFFF);
  static const Color primaria = Color(0xFF271019);
  static const Color secundaria = Color(0xFF994BBD);
  static const Color desativado = Color(0xFF8A8A8A);
  static const Color textFieldBorder = Color(0xFFC4C4C4);
  static const Color labelTextField = Color(0xFF545759);
  static const Color erroBorder = Color(0xFFFFA9A9);
  static const Color erroText = Color(0xFFBD4E4E);
  static const Color searchIcon = Color(0xFF7E7E7F);
  static const Color searchText = Color(0xFF1A1B1C);
  static const Color empresaNome = Color(0xFF939393);
  static const Color mostrarSenha = Color(0xFF767677);
}
