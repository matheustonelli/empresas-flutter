class ApiPaths {
  static const String scheme = 'https';
  static const String host = 'empresas.ioasys.com.br';
  static const String apiVersion = '/api/v1';
  static const String login = '$apiVersion/users/auth/sign_in';
  static const String buscarEmpresas = '$apiVersion/enterprises';
  static const String fotoEmpresa = '$scheme://$host';

}