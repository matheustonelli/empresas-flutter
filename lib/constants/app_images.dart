class AppImages {
  static const String logoIoasys = 'lib/assets/imagens/logo_ioasys.png';
  static const String logoIoasysVector = 'lib/assets/imagens/ioasys_logo_vector.png';
  static const String background = 'lib/assets/imagens/background.png';
  static const String backgroundLogin = 'lib/assets/imagens/background_login.png';
  static const String empresaNaoEncontrada = 'lib/assets/imagens/empresa_nao_encontrada.png';
}
