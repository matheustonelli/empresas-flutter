import 'package:empresas_flutter/domain/entities/usuario/usuario.dart';
import 'package:empresas_flutter/pages/home/home_page.dart';
import 'package:empresas_flutter/utils/services/rest_api_service/requests/login/login_request.dart';
import 'package:empresas_flutter/utils/ui/app_dialogs/app_dialogs.dart';
import 'package:flutter/material.dart';

class Login {

  static Future<void> login(BuildContext context, GlobalKey<FormState> formKey, String email, String senha) async {
    if (formKey.currentState!.validate()) {
      LoginRequest loginRequest = LoginRequest();

      AppDialogs.carregandoDialog(context);

      Usuario? usuario = await loginRequest.autenticar(email, senha);
      Navigator.pop(context);

      if (usuario != null) {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => HomePage(
              usuario: usuario,
            ),
          ),
        );
      } else {
        AppDialogs.loginInvalidoDialog(context);
      }
    }
  }
}