import 'package:http/http.dart';

abstract class IHttpService {

  Future<Response> get(String url, Map<String, String>? headers, Map<String, dynamic>? query);
  Future<Response> post(String url, dynamic body);

}
