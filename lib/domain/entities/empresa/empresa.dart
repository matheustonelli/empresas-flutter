class Empresa {
  int id;
  String nome;
  String foto;
  String descricao;
  String tipoEmpresa;

  Empresa(
    this.id,
    this.nome,
    this.foto,
    this.descricao,
    this.tipoEmpresa,
  );

  factory Empresa.fromMap(Map<String, dynamic> map) {
    return Empresa(
      map['id'],
      map['enterprise_name'],
      map['photo'],
      map['description'],
      map['enterprise_type']['enterprise_type_name'],
    );
  }
}
